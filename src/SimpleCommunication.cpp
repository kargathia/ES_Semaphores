#include <fcntl.h> /* For O_* constants */
#include <semaphore.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <unistd.h>
#include <iostream>

typedef struct cipher {
  int number;
  char pronunciation[20];
} cipher_t;

void Reader(void* vaddr) {
  sem_t* sem = sem_open("semmy", O_CREAT, S_IRUSR | S_IWOTH, 10);
  if (sem == NULL) {
    perror("failed to open semaphore");
    return;
  }
  setsid();
  const int numVals = 10;
  cipher_t* shmCipher = (cipher_t*)vaddr;

  int numRead = 0;

  for (int i = 0; i < numVals * 100; ++i) {
    sem_wait(sem);
    numRead++;
    std::cout << shmCipher->number << " = " << shmCipher->pronunciation << " ("
              << numRead << ")" << std::endl;
  }
}

void Writer(void* vaddr) {
  sem_t* sem = sem_open("semmy", O_CREAT, S_IRUSR | S_IWUSR | S_IROTH, 10);
  if (sem == NULL) {
    perror("failed to open semaphore");
    return;
  }
  const int numVals = 10;
  const char* textVals[numVals] = {"null\0",  "one\0",  "two\0", "three\0",
                                   "four\0",  "five\0", "six\0", "seven\0",
                                   "eight\0", "nine\0"};

  cipher_t* shmCipher = (cipher_t*)vaddr;
  for (int i = 0; i < numVals * 100; ++i) {
    int current = i % numVals;
    shmCipher->number = current;
    strcpy(shmCipher->pronunciation, textVals[current]);
    std::cout << "Writing " << current << std::endl;
    sem_post(sem);
  }
}

int main() {
  int shm_fd;
  void* vaddr;
  const char* fileName = "shared_mem";

  // In case previous iteration crashed
  sem_unlink("semmy");

  /* get shared memory handle */
  if ((shm_fd = shm_open(fileName, O_CREAT | O_RDWR, 0666)) == -1) {
    perror("cannot open");
    return -1;
  }

  /* set the shared memory size to the size of one cipher_t */
  if (ftruncate(shm_fd, sizeof(cipher_t)) != 0) {
    perror("cannot set size");
    return -1;
  }

  /* Map shared memory in address space. MAP_SHARED flag tells that this is a
  * shared mapping */
  if ((vaddr = mmap(0, sizeof(cipher_t), PROT_WRITE | PROT_READ, MAP_SHARED,
                    shm_fd, 0)) == MAP_FAILED) {
    perror("cannot mmap");
    return -1;
  }

  /* lock the shared memory */
  if (mlock(vaddr, sizeof(cipher_t)) != 0) {
    perror("cannot mlock");
    return -1;
  }

  int processID = fork();
  if (processID < 0) {
    perror("Unable to fork");
    return -1;
  }

  if (processID == 0)  // client
  {
    Reader(vaddr);

    // As the reader waits, it will be the last one out.
    /* unmap from address space */
    munmap(vaddr, sizeof(cipher_t));
    close(shm_fd);
    shm_unlink(fileName);
    sem_unlink("semmy");
  } else {
    Writer(vaddr);
  }

  return 0;
}