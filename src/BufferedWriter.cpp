#include "SemHandler.h"
#include "SharedHandler.h"

namespace {
const char* readerSemName = "semmy";
const char* writerSemName = "sammy";
const char* fileName = "shammy";

const int numCiphers = 10;
const int numVals = 10;
const int numIterations = 100;
const int headstartSize = numCiphers - 1;

const char* invalidText = "invalid";
const char* textVals[numVals] = {"null", "one", "two",   "three", "four",
                                 "five", "six", "seven", "eight", "nine"};
}

void Writer() {
  int fileSize = sizeof(cipher_t) * numCiphers;
  SharedHandler shared(fileName, fileSize);
  SemHandler semW(writerSemName, headstartSize);
  SemHandler semR(readerSemName, 0);

  // Will be the first entry in an array of ciphers
  cipher_t* shmCiphers = shared.GetShared();
  for (int it = 0; it < numIterations; ++it) {
    for (int i = 0; i < numVals; ++i) {
      // Wait for reader to signal this cipher is available for writing
      sem_wait(semW.Get());

      // Get pointer to current cipher from the array of ciphers
      cipher_t* currCipher = &shmCiphers[i];

      // Set appropriate values to current cipher
      currCipher->number = i;
      strcpy(currCipher->pronunciation, textVals[i]);

      // Write output 
      std::cout << "Writing " << i << std::endl;

      // Signal reader that this entry was written
      sem_post(semR.Get());
    }
  }
  // Eat all surplus posts that gave writer a head start
  for (int i = 0; i < headstartSize; ++i) {
    sem_wait(semW.Get());
  }

  // Notify reader that the writer is done
  sem_post(semR.Get());
  // Wait for reader to signal it is also done
  sem_wait(semW.Get());
}

int main() {
  Writer();
  return 0;
}