#include "SemHandler.h"
#include "SharedHandler.h"

namespace {
const char* readerSemName = "semmy";
const char* writerSemName = "sammy";
const char* fileName = "shammy";

const int numCiphers = 10;
const int numVals = 10;
const int numIterations = 100;
const int headstartSize = numCiphers - 1;

const char* invalidText = "invalid";
const char* textVals[numVals] = {"null", "one", "two",   "three", "four",
                                 "five", "six", "seven", "eight", "nine"};
}

void Reader() {
  int fileSize = sizeof(cipher_t) * numCiphers;
  SharedHandler shared(fileName, fileSize);
  SemHandler semW(writerSemName, headstartSize); 
  SemHandler semR(readerSemName, 0);

  std::cout << "Waiting for writer..." << std::endl;

  // Will be a pointer to the first item in an array
  cipher_t* shmCiphers = shared.GetShared();
  for (int it = 0; it < numIterations; ++it) {
    for (int i = 0; i < numVals; ++i) {
      // Wait for the writer thread to notify that this cipher is available
      sem_wait(semR.Get());

      // Get current item in the cipher array
      cipher_t* currCipher = &shmCiphers[i];

      // Prints current value
      std::cout << currCipher->number << " = " << currCipher->pronunciation
                                           << std::endl;

      // Set invalid values to read cipher to demonstrate we are always reading
      // unique values
      currCipher->number = -1;
      strcpy(currCipher->pronunciation, invalidText);

      // Signal the writer thread that this entry was read
      sem_post(semW.Get());
    }
  }

  // Notify writer the reader is completely done
  sem_post(semW.Get());
  // Wait for writer to signal it is also done
  sem_wait(semR.Get());
}

int main() {
  Reader();

  return 0;
}