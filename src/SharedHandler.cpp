#include "SharedHandler.h"

SharedHandler::SharedHandler(std::string file, int size)
    : fileName(file), bufferSize(size) {
      SetupShared();
    }

SharedHandler::~SharedHandler() {
  if (vaddr != NULL) {
    munmap(vaddr, bufferSize);
    close(sharedFD);
    shm_unlink(fileName.c_str());
  }
}

bool SharedHandler::SetupShared() {
  /* get shared memory handle */
  if ((sharedFD = shm_open(fileName.c_str(), O_CREAT | O_RDWR, 0666)) == -1) {
    perror("cannot open");
    return false;
  }

  /* set the shared memory size to the size of one cipher_t */
  if (ftruncate(sharedFD, bufferSize) != 0) {
    perror("cannot set size");
    return false;
  }

  /* Map shared memory in address space. MAP_SHARED flag tells that this is a
  * shared mapping */
  if ((vaddr = mmap(0, bufferSize, PROT_WRITE | PROT_READ, MAP_SHARED, sharedFD,
                    0)) == MAP_FAILED) {
    perror("cannot mmap");
    return false;
  }

  /* lock the shared memory */
  if (mlock(vaddr, bufferSize) != 0) {
    perror("cannot mlock");
    return false;
  }
  return true;
}