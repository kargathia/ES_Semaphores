CC = g++
CFLAGS = -O2 -Wall -Werror -pedantic
LDDFLAGS = -lrt -pthread -g
INCLUDE = -Iinc

SOURCES = SemHandler SharedHandler
MAINS = SimpleCommunication BufferedReader BufferedWriter
OBJ = $(addprefix obj/, $(addsuffix .o, $(SOURCES)))

.PHONY: makedir

all: makedir $(MAINS)

makedir:
	@[ -e bin ] || mkdir bin
	@[ -e obj ] || mkdir obj

clean:
	@rm -rf bin obj *.txt

$(OBJ): makedir
	@$(CC) $(CFLAGS) $(INCLUDE) -c $(subst obj/,src/,$(subst .o,.cpp,$@)) -o $@

$(MAINS): $(OBJ)
	$(CC) -o bin/$@.out src/$@.cpp $(OBJ) $(INCLUDE) $(LDDFLAGS)