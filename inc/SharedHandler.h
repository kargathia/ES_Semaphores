#pragma once

#include <fcntl.h> /* For O_* constants */
#include <semaphore.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <iostream>
#include <sstream>

#define SSTR(x)                                                             \
  static_cast<std::ostringstream&>((std::ostringstream() << std::dec << x)) \
      .str()

typedef struct cipher {
  int number;
  char pronunciation[20];
} cipher_t;

class SharedHandler {
 public:
  SharedHandler(std::string fileName, int size);
  virtual ~SharedHandler();
  bool SetupShared();
  cipher_t* GetShared() { return (cipher_t*)vaddr; }

 private:
  int sharedFD;
  void* vaddr;
  std::string fileName;
  int bufferSize;
};