#pragma once

#include <fcntl.h> /* For O_* constants */
#include <semaphore.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <iostream>
#include <sstream>

class SemHandler {
 public:
  SemHandler(std::string name, int count);
  virtual ~SemHandler();
  sem_t* Get() { return mySem; }

 private:
  sem_t* OpenSem(const char* address, int count);
  sem_t* mySem;
  std::string mySemName;
};